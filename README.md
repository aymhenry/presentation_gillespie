This repository contains 3 files:

- `Presentation.pdf`: Slides presented during the class.
- `Lokta voltera.pdf`: A exercise requiring you to code a integration algorithm and a Gillespie algorithm.
- `Lokta voltera.ipynb`: Solution of the exercise
